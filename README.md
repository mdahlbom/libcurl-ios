# libCURLiOS #

This project wraps curl 7.40.0 into a Cocoa Touch (iOS) Framework. It also builds openssl+libssh2 into the same binary and compiles curl to support them, enabling HTTPS/SFTP/FTPS connections out of the box.

For mere openssl+libssh2 support, see this awesome project: [https://github.com/x2on/libssh2-for-iOS](https://github.com/x2on/libssh2-for-iOS). It was used to build the binaries for this project's use also.

## Licence ##

libcurl is released under the MIT licence. See the project's website for more information: http://curl.haxx.se/

Any additional sources / configurations in this framework are available under the `Beer Licence`:

    The user is allowed to do anything with the licensed material. Should the user of the product meet 
    the author and consider the software useful, he is encouraged to buy the author a beer.

## HOWTO ##

Steps to build the framework:

1. Open the project file in Xcode
2. Select a simulator build (say, iPhone 6)
3. Edit the "libCURLiOS" scheme and change the "Run" Build Configuration to `Release`
4. Hit `Product > Build` 
5. Your framework will be built into the project root directory: `libCURLiOS.framework`

Note that this build will have a dependency to zlib, so you'll need to add "libz.dylib" to your project using this framework.

## Code examples ##

Use **#import <libCURLiOS/libCURLiOS.h>** to include the headers into your project. 

See the following links for code examples:

* http://curl.haxx.se/libcurl/c/example.html